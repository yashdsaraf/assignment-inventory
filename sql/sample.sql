-- Database: derby
-- Database name: inventory
-- Database username: nbuser
-- Database password: nbuser

create table "ITEM" (
    "ID" INTEGER not null primary key GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
    "NAME" VARCHAR(30),
    "CATEGORY" VARCHAR(50),
    "PRICE" INTEGER
);

insert into ITEM ("NAME", "CATEGORY", "PRICE") values
('pen', 'stationery', 10),
('pencil', 'stationery', 7),
('eraser', 'stationery', 5),
('marker', 'stationery', 15);
