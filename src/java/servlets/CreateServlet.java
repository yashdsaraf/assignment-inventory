/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.DBConnection;

/**
 *
 * @author rorschack
 */
@WebServlet(name = "CreateServlet", urlPatterns = {"/create"})
public class CreateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name, category;
        double price;
        name = request.getParameter("name");
        category = request.getParameter("cat");
        price = Double.parseDouble(request.getParameter("price"));
        try {
            Connection conn = DBConnection.getConnection();
            String query = "insert into " + DBConnection.TABLE +
                    " (\"NAME\", \"CATEGORY\", \"PRICE\") values (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, name);
            stmt.setString(2, category);
            stmt.setDouble(3, price);
            if (stmt.executeUpdate() > 0) {
                response.sendRedirect("index");
            } else {
                throw new SQLException("Delete operation failed");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendRedirect("index?error=" + ex.getMessage());
        }
    }


}
