package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import servlets.ListServlet;

public class DBConnection {
    
    private static final String URL = "jdbc:derby://localhost:1527/inventory;user=nbuser;password=nbuser";
    private static Connection conn;
    public static final String TABLE = "ITEM";
    
    private DBConnection() { }
    
    public static Connection getConnection() {
        conn = conn == null ? createConnection() : conn;
        return conn;
    }
    
    private static Connection createConnection() {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            return DriverManager.getConnection(URL);
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
