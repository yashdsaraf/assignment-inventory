<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit item</title>
<% ResultSet rs = (ResultSet) request.getAttribute("rs");%>
<h3>Enter updated item values -- </h3>
<form method="POST" action="update">
    Id: <input type="text" name="id" value="<%= rs.getString("id")%>" readonly /> <br><br>
    Name: <input type="text" name="name" value="<%= rs.getString("name")%>" required /> <br><br>
    Category: <input type="text" name="cat" value="<%= rs.getString("category")%>" required /> <br><br>
    Price: <input type="number" name="price" value="<%= rs.getString("price")%>" required /> <br><br>
    <input type="submit" value="Update" />
</form>
