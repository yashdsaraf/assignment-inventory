<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Item list</title>
<style>
    table, th, td {
        border: solid black 1px;
        border-collapse: collapse;
        padding: 10px;
        text-align: center;
    }
    h3 {
        color: red;
    }
</style>
<h3>${param.error}</h3>
<%
    ResultSet rs = (ResultSet) request.getAttribute("rs");
    if (!rs.next()) {
%>
<h1>No records exist!</h1>
<%
} else {
%>
<form method="GET">
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>CATEGORY</th>
                <th>PRICE (in RS.)</th>
            </tr>
        </thead>
        <tbody>
            <%
                do {
            %>
            <tr>
                <td>
                    <%
                        String id = rs.getString("ID");
                        out.println("<input type=\"radio\" name=\"id\" value=\"" + id + "\" />&nbsp; " + id);
                    %>
                </td>
                <td><%= rs.getString("NAME")%></td>
                <td><%= rs.getString("CATEGORY")%></td>
                <td><%= rs.getString("PRICE")%></td>
            </tr>
            <%
                } while (rs.next());
            %>
        </tbody>
    </table>
    <%
        }
    %>
    <br>
    <a href="new.html"><button type="button">New</button></a>
    <button type="submit" formaction="edit">Edit</button>
    <button type="submit" formaction="delete">Delete</button>
</form>